#include <stdio.h>

int fibonacci(int);
int fibonacciSeq(int);

int fibonacci(int n)
{
    if (n==0)
        return 0;
    else if (n==1)
        return 1;
    else
        return fibonacci(n-1)+fibonacci(n-2);
}

int fibonacciSeq(int n)
{
    int i=0,c;
    for (c = 1; c <= n; c++)
    {
    printf("%d\n",fibonacci(i));
    i++;
    }
}

int main()
{
   int n;
   printf("Input number for the Fibonacci series: ");
   scanf("%d",&n);
   fibonacciSeq(n);
   return 0;
}
