#include <stdio.h>

void func(int);

void func(int n)
 {
    int i,j;
    for(i=1; i<=n; i++)
    {
      for(j=i; j>=1; j--)
        {
            printf("%d", j);
        }
        printf("\n");
    }
 }

int main()
{
    int n;

    printf("Enter number: ");
    scanf("%d", &n);
    func(n);
    return 0;
}

